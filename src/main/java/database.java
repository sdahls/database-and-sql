import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

public class database {

    public static ArrayList getCustomers(){ //method for getting customers

        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite"; //URL string for connection to the database
        Connection conn = null;
        ArrayList<Customer> customers = new ArrayList<Customer>();//Arraylist with customer objects
        try{
            // Open Connection
            conn = DriverManager.getConnection(URL);
            //System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName FROM customer");//getting the customer ID and full name from the customer-table
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();//loading the result and putting it in the resultset

            // Process Results
            while (resultSet.next()) { //making customerobjects from the resultset
                customers.add(
                        new Customer(
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("CustomerId")

                        ));
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }

        return customers;//returning the arraylist of customers
    }

    public static ArrayList getCustomersMostPopularGenreById(String id){ //method for getting genres from a specific customer Id

        String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite"; //conectionstring
        Connection conn = null;
        ArrayList<genreCount> genres = new ArrayList<>();

        try{
            // Open Connection
            conn = DriverManager.getConnection(URL);
           // System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =//SQL-statement who connects the talbes and gets the genres and how many instances of them there is.
                    conn.prepareStatement("SELECT genre.Name, count(genre.Name)  FROM customer, invoice, invoiceline, track, genre where " +
                            " customer.customerId=invoice.customerId and " +
                            " invoice.InvoiceId=invoiceline.InvoiceId and " +
                            " invoiceline.trackId=track.trackId and " +
                            " track.genreId=genre.genreId and "+
                            " customer.CustomerId=? group by genre.Name order by count(genre.Name) desc");

            preparedStatement.setString(1, id); //passing the id to the query
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();//loading resultset

            // Process Results

            while (resultSet.next()) {  //putting the genres and the instances of them in an object specifically design for this task
                genres.add(new genreCount(
                        resultSet.getString("Name"),
                        resultSet.getInt(2)
                ));
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return genres;
    }


    public static void main(String[] args) {
            ArrayList<Customer> customers=new ArrayList<>();
            ArrayList<genreCount> genreCounts = new ArrayList<>();
            String id="";//initiating
            int random;
            Random rnd=new Random();
            int mostPopularGenreCount;
            customers= getCustomers();//getting all the customers and putting in an array


            if(args.length==0){//if the argument is empty

               random=rnd.nextInt(customers.size()); //getting a random int based on the size of the customer array
               id=Integer.toString(random);//assigning the random int to the id
            }else {
                id=args[0];//if its longer than zero then assign the args to id
            }


        for(Customer customer: customers){//itterating through the customers and if Id matches then print the name
            if(customer.getCustomerId().equals(id))
            System.out.println("The customers name is: "+customer.getFirstName() + " " +  customer.getLastName());
        }

        genreCounts=getCustomersMostPopularGenreById(id); //getting the array of genres and number of instances

        mostPopularGenreCount=genreCounts.get(0).getCount();//since the array is orderd with most popular in firs place (index 0), then pass the number to mostpopgen-
        for(genreCount instanceOfgenre: genreCounts){ //itterating through the array
            if(mostPopularGenreCount==instanceOfgenre.getCount()){ //IF it is as great as most popular ie tie, then print
                System.out.println("The most popular genre for this user is: "  + instanceOfgenre.getGenre());
            }

        }




    }
}
